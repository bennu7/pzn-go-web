package pzn_go_web

import (
	"fmt"
	"net/http"
	"testing"
)

// *HandlerFunc ini tidak mendukung pembuatan endpoint
func TestHandler(t *testing.T) {
	var handler http.HandlerFunc = func(writer http.ResponseWriter, request *http.Request) {
		_, err := fmt.Fprint(writer, "Hello Golang")
		if err != nil {
			return
		}
	}

	server := http.Server{
		Addr:    "localhost:8080",
		Handler: handler,
	}

	err := server.ListenAndServe()
	if err != nil {
		return
	}
}
