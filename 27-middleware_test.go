package pzn_go_web

import (
	"fmt"
	"net/http"
	"testing"
)

type LogMiddleware struct {
	Handler http.Handler
}

type ErrorHandlerMiddleware struct {
	Handler http.Handler
}

func (middleware *LogMiddleware) ServeHTTP(writer http.ResponseWriter, request *http.Request) {
	fmt.Println("Before Execute Handler")

	middleware.Handler.ServeHTTP(writer, request)

	fmt.Println("After Execute Handler")
}

func (errorHandlerMiddleware *ErrorHandlerMiddleware) ServeHTTP(writer http.ResponseWriter, request *http.Request) {
	defer func() {

		err := recover()
		fmt.Println("recover : ", err)
		if err != nil {
			writer.WriteHeader(http.StatusInternalServerError)
			fmt.Fprintf(writer, "Error oii : %s", err)
		}
	}()

	errorHandlerMiddleware.Handler.ServeHTTP(writer, request)
}

func TestMiddlewareServer(t *testing.T) {
	mux := http.NewServeMux()

	mux.HandleFunc("/", func(writer http.ResponseWriter, request *http.Request) {
		fmt.Println("midd")
		fmt.Fprint(writer, "is this middleware!")
	})

	mux.HandleFunc("/oii", func(writer http.ResponseWriter, request *http.Request) {
		fmt.Println("midd nya oiii")
		fmt.Fprint(writer, "is this middleware oii!")
	})

	// *example error Handler
	mux.HandleFunc("/panic", func(writer http.ResponseWriter, request *http.Request) {
		fmt.Println("midd error")
		panic("oiii help error")
	})

	// *middleware
	//logMiddleware := new(LogMiddleware)
	//logMiddleware.Handler = mux
	//logMiddleware := &LogMiddleware{
	//	Handler: mux,
	//}

	// *error handler dengan middleware
	errorHandler := &ErrorHandlerMiddleware{
		Handler: &LogMiddleware{
			Handler: mux,
		},
	}

	server := http.Server{
		Addr: "localhost:8080",
		//Handler: logMiddleware,
		Handler: errorHandler,
	}

	err := server.ListenAndServe()
	if err != nil {
		panic(err)
	}
}
