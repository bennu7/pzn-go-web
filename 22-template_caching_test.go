package pzn_go_web

import (
	"embed"
	"fmt"
	"io"
	"net/http"
	"net/http/httptest"
	"testing"
	"text/template"
)

// *lebih di awal parse dengan memanfaatkan embed dan ParseFS agar mendapatkan caching
// !lakukan deklarasi template di luar function, bukan di dalam function dengan memanfaatkan variabel global

//go:embed templates/*.gohtml
var templatess embed.FS

var myTemplates = template.Must(template.ParseFS(templatess, "templates/*.gohtml"))

func templateCaching(w http.ResponseWriter, r *http.Request) {
	myTemplates.ExecuteTemplate(w, "simple.gohtml", "woi HTML Template!")
}

func TestTemplateCaching(t *testing.T) {
	request := httptest.NewRequest(http.MethodGet, "http://localhost:8080", nil)
	recorder := httptest.NewRecorder()

	templateCaching(recorder, request)

	response := recorder.Result()
	body, _ := io.ReadAll(response.Body)
	fmt.Println(string(body))
}
