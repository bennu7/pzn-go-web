package pzn_go_web

import (
	"fmt"
	"io"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"
	"text/template"
)

// *04 Function Pipelines
// *result function yang akan di oper ke function berikutnya
func templateFunctionPipeline(w http.ResponseWriter, r *http.Request) {
	t := template.New("FUNCTION")
	t = t.Funcs(map[string]any{
		"sayHello": func(value string) string {
			return "woiii " + value
		},
		"upper": func(value string) string {
			return strings.ToUpper(value)
		},
		"lower": func(value string) string {
			return strings.ToLower(value)
		},
	})

	t = template.Must(t.Parse(`{{sayHello .Name | lower | upper}}`))

	t.ExecuteTemplate(w, "FUNCTION", Page{
		Name: "this function loop",
	})
}

// *03 add new global Function, name upper
// *diimplementasikan sebelum dilakukan parsing template
func templateFunctionMapAddFunc(w http.ResponseWriter, r *http.Request) {
	t := template.New("FUNCTION")
	t = t.Funcs(map[string]any{
		"upper": func(value string) string {
			return strings.ToUpper(value)
		},
	})

	t = template.Must(t.Parse(`{{upper .Name}}`))

	t.ExecuteTemplate(w, "FUNCTION", map[string]any{
		"Name": "convert me to uppercase",
	})
}

// *02 global function ini adalah len, source : https://github.com/golang/go/blob/master/src/text/template/funcs.go pada FuncMap
func templateFunctionGlobal(w http.ResponseWriter, r *http.Request) {
	t := template.Must(template.New("FUNCTION").Parse(`count LEN : {{len .Name}}`))

	t.ExecuteTemplate(w, "FUNCTION", map[string]any{
		"Name": "that's template function global!",
	})
}

type MyPage struct {
	Name string
}

func (myPage MyPage) SayFuihh(name string) string {
	return "Hello " + name + ", who is stupid " + myPage.Name + "?"
}

func (myPage MyPage) SayWkwk(name string) string {
	return "Coooo " + name + ", who is stupid " + myPage.Name + "?"
}

// *01 Template Function
func templateFunction(w http.ResponseWriter, r *http.Request) {
	t := template.Must(template.New("FUNCTION").Parse(`{{.SayFuihh "lurrr"}}`))

	t.ExecuteTemplate(w, "FUNCTION", MyPage{
		Name: "ben",
	})
}

func TestTemplateFunction(t *testing.T) {
	request := httptest.NewRequest(http.MethodGet, "http://localhost:8080", nil)
	recorder := httptest.NewRecorder()

	//templateFunction(recorder, request)
	//templateFunctionGlobal(recorder, request)
	// templateFunctionMapAddFunc(recorder, request)
	templateFunctionPipeline(recorder, request)

	response := recorder.Result()
	body, _ := io.ReadAll(response.Body)
	fmt.Println(string(body))
}
