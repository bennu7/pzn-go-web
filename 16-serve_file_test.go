package pzn_go_web

import (
	_ "embed"
	"fmt"
	"net/http"
	"testing"
)

// *ingin menggunakan static file sesuai dengan yang kita inginkan dengan memanfaatkan http.ServeFile()
// *Dengan menggunakan function ini, kita bisa menentukan file mana yang ingin kita tulis ke http response
func serverFile(w http.ResponseWriter, r *http.Request) {
	nameQuery := r.URL.Query().Get("name")
	if nameQuery != "" {
		http.ServeFile(w, r, "./resources/ok.html")
	} else {
		http.ServeFile(w, r, "./resources/notFound.html")
	}
}

// ?memanfaatkan Golang Embed, gunakan fmt untuk mengirim hasil valuenya

//go:embed resources/ok.html
var OK string

//go:embed resources/notFound.html
var notFound string

func serveFileEmbed(w http.ResponseWriter, r *http.Request) {
	nameQuery := r.URL.Query().Get("name")
	if nameQuery != "" {
		fmt.Fprint(w, OK)
	} else {
		fmt.Fprintf(w, notFound)
	}
}
func TestServeFileServer(t *testing.T) {
	mux := http.NewServeMux()

	//mux.HandleFunc("/hei", serverFile)
	mux.HandleFunc("/hei", serveFileEmbed)
	server := http.Server{
		Addr:    "localhost:8080",
		Handler: mux,
	}

	err := server.ListenAndServe()
	if err != nil {
		panic(err)
	}
}
