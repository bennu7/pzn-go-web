package pzn_go_web

import (
	"fmt"
	"io"
	"net/http"
	"net/http/httptest"
	"testing"
	"time"
)

// *Create Cookie
func setCookie(w http.ResponseWriter, r *http.Request) {
	cookie := new(http.Cookie)
	cookie.Name = "X-LIH-name"
	valueQuery := r.URL.Query().Get("name")
	if valueQuery == "" {
		w.WriteHeader(http.StatusNotAcceptable)
		fmt.Fprint(w, "no cookie are send")
		return
	}
	cookie.Value = r.URL.Query().Get("name")
	cookie.Expires = time.Now().Add(10 * time.Minute)
	cookie.Path = "/" // *aktifnya cookie Path ini berlaku di main endpoint dan childnya, jika di atur set /profile maka akan aktif di endpoint /profile dan di child nya

	// *example you can add second cookie
	secondCookie := new(http.Cookie)
	secondCookie.Name = "second-cookie"
	secondCookie.Value = r.URL.Query().Get("age")
	secondCookie.Path = "/"

	// *set and send this cookie
	http.SetCookie(w, cookie)
	http.SetCookie(w, secondCookie)
	fmt.Fprint(w, "Success create cookie")
}

// *Get Cookie
func getCookie(w http.ResponseWriter, r *http.Request) {
	//r.Cookies() // *r.Cookies() ini akan mengambil semua Cookie nya
	cookie, err := r.Cookie("X-LIH-name")
	secondCookie, err := r.Cookie("second-cookie")
	//fmt.Println("nil : ", err)
	if err != nil {
		fmt.Fprint(w, "No Cookie", err)
	} else {
		fmt.Fprintf(w, "Hei-hei %s is that your age is %s?", cookie.Value, secondCookie.Value)
		//fmt.Fprintf(w, "Hei-hei %s?", cookie.Value)
	}
}

func TestRunning(t *testing.T) {
	mux := http.ServeMux{}
	mux.HandleFunc("/set-cookie", setCookie)
	mux.HandleFunc("/get-cookie", getCookie)

	server := http.Server{
		Addr:    "localhost:8080",
		Handler: &mux,
	}

	err := server.ListenAndServe()
	if err != nil {
		panic(err)
	}
}

func TestCreateCookie(t *testing.T) {
	request := httptest.NewRequest(http.MethodGet, "http://localhost:8080/set-cookie?name=bennu&age=23", nil)
	recorder := httptest.NewRecorder()

	setCookie(recorder, request)

	cookies := recorder.Result().Cookies()

	// *perlu di looping karena Cookies() bersifat slice of pointer Cookie
	for _, cookie := range cookies {
		fmt.Printf("%s : %s \n", cookie.Name, cookie.Value)
	}
}

func TestGetCookie(t *testing.T) {
	request := httptest.NewRequest(http.MethodGet, "http://localhost:8080/", nil)
	cookie := new(http.Cookie)
	cookie.Name = "X-LIH-name"
	cookie.Value = "bennu"
	request.AddCookie(cookie)

	secondCookie := new(http.Cookie)
	secondCookie.Name = "second-cookie"
	secondCookie.Value = "23"
	request.AddCookie(secondCookie)

	recorder := httptest.NewRecorder()

	getCookie(recorder, request)

	response := recorder.Result()
	body, err := io.ReadAll(response.Body)
	if err != nil {
		println("err ReadALl ", err)
	}
	fmt.Println(string(body))
}
