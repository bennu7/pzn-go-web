package pzn_go_web

import (
	"fmt"
	"io"
	"net/http"
	"net/http/httptest"
	"net/url"
	"strings"
	"testing"
)

func TestQueryParemeter(t *testing.T) {
	// *single query
	//request := httptest.NewRequest(http.MethodGet, "http://localhost:8080/hei?name=ibnu", nil)
	// *multiple query
	//request := httptest.NewRequest(http.MethodGet, "http://localhost:8080/hei?first_name=ibnu&last_name=lalu", nil)
	// *multiple query for key same
	request := httptest.NewRequest(http.MethodGet, "http://localhost:8080/hei?name=ibnu&name=lalu&name=deri", nil)
	recorded := httptest.NewRecorder()

	//QueryParam(recorded, request)
	//multipleQueryParameter(recorded, request)
	multipleQueryParameterSameKey(recorded, request)

	response := recorded.Result()
	body, _ := io.ReadAll(response.Body)
	fmt.Println(string(body))
}

// *single query parameter
func QueryParam(writer http.ResponseWriter, request *http.Request) {
	name := request.URL.Query().Get("name")
	if name == "" {
		fmt.Fprintf(writer, "no value")
	} else {
		fmt.Fprintf(writer, "heii %s", name)
	}
}

// *multiple query parameter
func multipleQueryParameter(writer http.ResponseWriter, r *http.Request) {
	firstName := r.URL.Query().Get("first_name")
	lastName := r.URL.Query().Get("last_name")

	fmt.Fprintf(writer, "heii %s %s", firstName, lastName)
}

// *multiple query paremeter for key same
// *dapat mengambil key yang sama pada query parameter dengan value yang berbeda"
func multipleQueryParameterSameKey(w http.ResponseWriter, r *http.Request) {
	var query url.Values = r.URL.Query()
	var names []string = query["name"]
	fmt.Fprintf(w, strings.Join(names, " "))
}
