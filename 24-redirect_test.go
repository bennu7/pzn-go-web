package pzn_go_web

import (
	"fmt"
	"net/http"
	"testing"
)

func redirectTo(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "that's Redirect page from url /rediret-from to /redirect-to")
}
func redirectFrom(w http.ResponseWriter, r *http.Request) {
	// *bisa dimafaatkan untuk redirect ke luar main route
	//http.Redirect(w, r, "https://www.google.com", http.StatusPermanentRedirect)

	http.Redirect(w, r, "/redirect-to", http.StatusPermanentRedirect)
}

func TestRedirectServer(t *testing.T) {
	mux := http.NewServeMux()

	mux.HandleFunc("/redirect-from", redirectFrom)
	mux.HandleFunc("/redirect-to", redirectTo)
	server := http.Server{
		Addr:    "localhost:8080",
		Handler: mux,
	}

	err := server.ListenAndServe()
	if err != nil {
		panic(err)
	}
}
