package pzn_go_web

import (
	"fmt"
	"io"
	"net/http"
	"net/http/httptest"
	"testing"
)

/*
* NOTE : untuk get header tidak Case Sensitive, sementara untuk get query paramater baru Case Sensitive
* untuk request header biasa dapat manfaatkan request, sementara custom header menggunakan responseWriter
 */
func TestHeader(t *testing.T) {
	request := httptest.NewRequest(http.MethodGet, "localhost:8080/", nil)
	recorder := httptest.NewRecorder()
	// *menambahkan header biasa
	//request.Header.Add("Content-Type", "application/json")

	// *get customisasi header
	requestHeader(recorder, request)
	poweredBy := recorder.Header().Get("x-powered-by")
	fmt.Println("poweredBy : ", poweredBy)

	response := recorder.Result()
	body, _ := io.ReadAll(response.Body)
	fmt.Println(string(body))
}

func requestHeader(w http.ResponseWriter, r *http.Request) {
	// *menambahkan header biasa gunakan request
	//contentType := r.Header.Get("Content-Type")
	//fmt.Fprintf(w, contentType)

	//	*menambabhkan header pada response (customisasi), guanakan responseWriter
	w.Header().Add("X-Powered-By", "Lalu Ibnu Hidayatullah")
	fmt.Fprintf(w, "OK")
}
