package pzn_go_web

import (
	"fmt"
	"io"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"
)

func TestFormPost(t *testing.T) {
	requestBody := strings.NewReader("first_name=lalu&last_name=ibnu")
	request := httptest.NewRequest(http.MethodPost, "localhost:8080/", requestBody)
	request.Header.Add("Content-Type", "application/x-www-form-urlencoded")
	recorder := httptest.NewRecorder()

	formPost(recorder, request)

	response := recorder.Result()
	body, err := io.ReadAll(response.Body)
	if err != nil {
		panic(err)
	}
	fmt.Println("body : ", string(body))

}

/*
* terlebih dauhulu ParseForm untuk pemeriksaan jika yang dikirim adalah body
* setelah itu baru ambil value body dari PostForm.Gey(key)
 */
func formPost(w http.ResponseWriter, r *http.Request) {
	err := r.ParseForm()
	if err != nil {
		panic(err)
	}

	firstName := r.PostForm.Get("first_name")
	lastName := r.PostForm.Get("last_name")
	fmt.Fprintf(w, "%s %s", firstName, lastName)

	// ?alternatif dapat menggunakan PostFormValue tanpa ParseForm, karena didalam PostFormValue ini terdapat ParseForm bernama ParseMultipartForm
	//first := r.PostFormValue("first_name")
	//last := r.PostFormValue("last_name")
	//fmt.Fprintf(w, "%s %s", first, last)
}
