package pzn_go_web

import (
	"embed"
	_ "embed"
	"fmt"
	"io"
	"net/http"
	"net/http/httptest"
	"testing"
	"text/template"
)

/*
* template data ini dapat kirim data yang berbeda-beda dengan mengambil Key dari .gohtml dan diisi pada method  ExecuteTemplate pada paramater ke 3
* diisi Key dan Value didalam map[string]any{}
* alternatif dapat dimanfaakan struct pengganti map[string]any{}
 */

//go:embed templates/name.gohtml
var nameHtml embed.FS

func templateData(w http.ResponseWriter, r *http.Request) {
	t := template.Must(template.ParseFS(nameHtml, "templates/*.gohtml"))

	// *menggunakan Mapping
	//t.ExecuteTemplate(w, "name.gohtml", map[string]any{
	//	"Title": "that's Stupid",
	//	"Name":  "Hello WkWk Land",
	//	"Address": map[string]any{
	//		"Street": "Kepoo anj!",
	//	},
	//})

	// *alternatif menggunkan Struct pada parameter ke 3
	t.ExecuteTemplate(w, "name.gohtml", Page{
		Title: "that's Stupid uu!",
		Name:  "Hello WkWk Landing",
		Address: Address{
			Street: "YIA",
		},
	})
}

type Page struct {
	Title string
	Name  string
	Address
}
type Address struct {
	Street string
	City   string
}

func TestTemplateData(t *testing.T) {
	mux := http.NewServeMux()

	mux.HandleFunc("/", templateData)
	server := http.Server{
		Addr:    "localhost:8080",
		Handler: mux,
	}

	err := server.ListenAndServe()
	if err != nil {
		panic(err)
	}
}

func TestTemplateDataTesting(t *testing.T) {
	request := httptest.NewRequest(http.MethodGet, "http://localhost:8080/", nil)
	recorder := httptest.NewRecorder()

	templateData(recorder, request)

	response := recorder.Result()
	result, err := io.ReadAll(response.Body)
	if err != nil {
		panic(err)
	}
	fmt.Println(response.Status)
	fmt.Println(string(result))
}
