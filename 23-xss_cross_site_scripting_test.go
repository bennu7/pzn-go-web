package pzn_go_web

import (
	"fmt"
	"html/template"
	"io"
	"net/http"
	"net/http/httptest"
	"testing"
)

// *myTemplates => var dari template caching
func templateAutoEscape(w http.ResponseWriter, r *http.Request) {
	myTemplates.ExecuteTemplate(w, "xss.gohtml", map[string]any{
		"Title": "Golang auto escape XSS",
		// *gunakan template.HTMLEscapeString atau template.HTMLEscaper()  agar tidak kemasukan value sembarangan
		// *jikalau ingin mengeksekusi html atau menonaktifkan xss tersebut gunakan template.HTML() atau langsung memasukkan value html nya
		// !jika tidak diperhatikan maka akan berdampak!
		"Body": template.HTMLEscaper("<h1 style='color: red'><script>alert('dont show me stupid!')</script>this heading 1</h1>"),
		//"Body": template.HTML(r.URL.Query().Get("name")),
	})
}

func templateAutoEscapeDisableXSS(w http.ResponseWriter, r *http.Request) {
	myTemplates.ExecuteTemplate(w, "xss.gohtml", map[string]any{
		"Title": "Golang auto escape XSS",
		// *sangat" tidak direkomendasikan dan terlalu berbahaya
		"Body": template.HTML(r.URL.Query().Get("name")),
	})
}

func TestTemplateXSSServer(t *testing.T) {
	mux := http.NewServeMux()

	//mux.HandleFunc("/", templateAutoEscape)
	mux.HandleFunc("/", templateAutoEscapeDisableXSS)
	server := http.Server{
		Addr:    "localhost:8080",
		Handler: mux,
	}

	err := server.ListenAndServe()
	if err != nil {
		panic(err)
	}
}

func TestTemplateXss(t *testing.T) {
	request := httptest.NewRequest(http.MethodGet, "http://localhost:8080?name=<p><script>alert('shitbug!')</script></p>", nil)
	recorder := httptest.NewRecorder()

	templateAutoEscape(recorder, request)

	response := recorder.Result()
	body, _ := io.ReadAll(response.Body)
	fmt.Println(string(body))
}
