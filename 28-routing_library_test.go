package pzn_go_web

import (
	"fmt"
	"github.com/julienschmidt/httprouter"
	"log"
	"net/http"
	"testing"
)

/*
? serveMux kurang advanced untuk advanced fitur seperti path variable, auto binding parameter dan middleware
* daftar alternatif mux lainnya :
* https://github.com/julienschmidt/httprouter
* https://github.com/gorilla/mux
* https://github.com/julienschmidt/go-http-routing-benchmark
*/

func Index(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	fmt.Fprint(w, "Welcome!\n")
}

func Hello(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	fmt.Fprintf(w, "hello, %s!\n", ps.ByName("name"))
}

func TestRoutingLibrary(t *testing.T) {
	router := httprouter.New()

	router.GET("/", Index)
	router.GET("/hei/:name", Hello)

	log.Fatal(http.ListenAndServe(":8080", router))
}
