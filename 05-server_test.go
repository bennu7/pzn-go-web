package pzn_go_web

import (
	"fmt"
	"net/http"
	"testing"
)

func TestServer(t *testing.T) {
	address := "localhost:8080"
	server := http.Server{
		Addr: address,
	}

	err := server.ListenAndServe()
	if err != nil {
		panic(err)
	}

	fmt.Println("running on ", address)
}
