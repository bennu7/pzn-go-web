package pzn_go_web

import (
	"fmt"
	"io"
	"net/http"
	"net/http/httptest"
	"testing"
)

// *http test : dapat dimanfaatkan testing handler web doi golang tanpa aplikasi web
func TestHttpTest(t *testing.T) {
	//request := httptest.NewRequest("GET", "http://localhost:8080", nil)
	request := httptest.NewRequest(http.MethodGet, "http://localhost:8080/hello", nil)
	recorder := httptest.NewRecorder()

	helloHandler(recorder, request)

	// *menangkap value menggunakan recorder
	//body := recorder.Body
	//fmt.Println(body)
	response := recorder.Result()
	body, err := io.ReadAll(response.Body)
	if err != nil {
		fmt.Println(err)
	}

	fmt.Println(response.StatusCode)
	fmt.Println(response.Status)
	fmt.Println(string(body))
}

func helloHandler(writer http.ResponseWriter, request *http.Request) {
	fmt.Fprintln(writer, "hello http test")
}
