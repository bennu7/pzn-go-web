package pzn_go_web

import (
	"embed"
	_ "embed"
	"fmt"
	"io"
	"net/http"
	"net/http/httptest"
	"testing"
	"text/template"
)

// *04 template golang embed, memanfaatkan embed

//go:embed templates/*.gohtml
var templates embed.FS

func templateGoEmbed(w http.ResponseWriter, r *http.Request) {
	// *ParseFS() butuh 2 parameter, source file embed dan string source file path
	t := template.Must(template.ParseFS(templates, "templates/*.gohtml"))

	t.ExecuteTemplate(w, "simple.gohtml", "source from go embed")
}

// *03 template directory tanpa memanggilkan nama file satu per satu, dengan memanfaatkan template.ParseGlob()
func templateDirectory(w http.ResponseWriter, r *http.Request) {
	t := template.Must(template.ParseGlob("./templates/*.gohtml"))

	// *jika file tidak ditemukan maka kembalian berupa string kosong
	t.ExecuteTemplate(w, "simple.gohtml", "wkwkLand")
}

// *02 memanfaatkan file external
func simpleHtmlFile(w http.ResponseWriter, r *http.Request) {
	t := template.Must(template.ParseFiles("./templates/simple.gohtml"))

	// *parameter kedua merupakan nama template nya mengikuti nama file
	t.ExecuteTemplate(w, "simple.gohtml", "Hello GOGO template")
}

// *01 simple HTML tanpa file html external
func simpleHTML(w http.ResponseWriter, r *http.Request) {
	templateText := `<html><body>{{.}}</body></html>`
	queryTemplate := r.URL.Query().Get("t")
	//t, err := template.New("SIMPLE").Parse(templateText)
	//if err != nil {
	//	panic(err)
	//}
	// *tidak perlu lagi handle Error template.New menggunakan t.Must, karena di dalam Must sudah ada handle error nya berisi panic()
	t := template.Must(template.New("SIMPLE").Parse(templateText))

	// *parameter kedua merupakan nama template nya
	//t.ExecuteTemplate(w, "SIMPLE", "heiehieii")
	t.ExecuteTemplate(w, "SIMPLE", queryTemplate)
}

func TestTemplate(t *testing.T) {
	mux := http.NewServeMux()

	//mux.HandleFunc("/", simpleHTML)
	//mux.HandleFunc("/", simpleHtmlFile)
	mux.HandleFunc("/", templateGoEmbed)
	server := http.Server{
		Addr:    "localhost:8080",
		Handler: mux,
	}

	err := server.ListenAndServe()
	if err != nil {
		panic(err)
	}
}

func TestTemplateTesting(t *testing.T) {
	request := httptest.NewRequest(http.MethodGet, "http://localhost:8080/hei?t=ben", nil)
	recorder := httptest.NewRecorder()

	//simpleHTML(recorder, request)
	//templateDirectory(recorder, request)
	templateGoEmbed(recorder, request)

	response := recorder.Result()
	result, err := io.ReadAll(response.Body)
	if err != nil {
		panic(err)
	}
	fmt.Println(response.Status)
	fmt.Println(string(result))
}
