package pzn_go_web

import (
	"fmt"
	"net/http"
	"testing"
)

// *dengan ServeMux ini mendukung pembuatan endpoint
func TestServeMux(t *testing.T) {
	mux := http.ServeMux{}

	//mux.HandleFunc("/", func(writer http.ResponseWriter, request *http.Request) {
	//	fmt.Fprintf(writer, "Hello endpoint /")
	//})
	mux.HandleFunc("/hi", func(writer http.ResponseWriter, request *http.Request) {
		fmt.Fprintf(writer, "Hello endpoint /hi")
	})

	mux.HandleFunc("/hi/thumbnail", func(writer http.ResponseWriter, request *http.Request) {
		fmt.Fprintf(writer, "Hello endpoint /hi/thumbnail")
	})

	server := http.Server{
		Addr:    "localhost:8080",
		Handler: &mux,
	}

	err := server.ListenAndServe()
	if err != nil {
		panic(err)
	}
}
