package pzn_go_web

import (
	"embed"
	"fmt"
	"io"
	"net/http"
	"net/http/httptest"
	"testing"
	"text/template"
)

// *memanfaatkan nested struct
func templateActionWith(w http.ResponseWriter, r *http.Request) {
	t := template.Must(template.ParseFiles("./templates/with.gohtml"))

	t.ExecuteTemplate(w, "with.gohtml", map[string]any{
		"Title": "Template With",
		"Name":  "wkwk Land",
		"Address": Address{
			Street: "Merapi",
			City:   "Sleman",
		},
	})
}

// *dapat digunakan range untuk mengiterasi tiap data array, slice, map atau channel
func templateActionRange(w http.ResponseWriter, r *http.Request) {
	t := template.Must(template.ParseFiles("./templates/range.gohtml"))
	t.ExecuteTemplate(w, "range.gohtml", map[string]any{
		"Title": `Looping ampe muter"`,
		"Hobby": []any{
			//"badminton", "running", "billiard",
		},
	})
}

//go:embed templates/comparator.gohtml
var comparatorHtml embed.FS

func templateActionComparator(w http.ResponseWriter, r *http.Request) {
	t := template.Must(template.ParseFS(comparatorHtml, "templates/*.gohtml"))

	t.ExecuteTemplate(w, "comparator.gohtml", map[string]any{
		"Title":      "operator ini co!",
		"FinalValue": 59,
	})
}

//go:embed templates/if.gohtml
var ifHtml embed.FS

func templateActionIf(w http.ResponseWriter, r *http.Request) {
	t := template.Must(template.ParseFS(ifHtml, "templates/*.gohtml"))

	t.ExecuteTemplate(w, "if.gohtml", map[string]any{
		//"Name": "tonjok gua bang",
		"Title": "idiihh",
	})
}

func TestTemplateAction(t *testing.T) {
	mux := http.NewServeMux()

	//mux.HandleFunc("/", templateActionIf)
	//mux.HandleFunc("/", templateActionRange)
	mux.HandleFunc("/", templateActionWith)
	server := http.Server{
		Addr:    "localhost:8080",
		Handler: mux,
	}

	err := server.ListenAndServe()
	if err != nil {
		panic(err)
	}
}

func TestTemplateActionTesting(t *testing.T) {
	request := httptest.NewRequest(http.MethodGet, "http://localhost:8080/", nil)
	recorder := httptest.NewRecorder()

	//templateActionIf(recorder, request)
	//templateActionComparator(recorder, request)
	//templateActionRange(recorder, request)
	templateActionWith(recorder, request)

	response := recorder.Result()
	result, err := io.ReadAll(response.Body)
	if err != nil {
		panic(err)
	}
	fmt.Println(response.Status)
	fmt.Println(string(result))
}
