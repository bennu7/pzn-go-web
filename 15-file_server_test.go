package pzn_go_web

import (
	"embed"
	_ "embed"
	"io/fs"
	"net/http"
	"testing"
)

//go:embed resources
var resources embed.FS

// *02 FileServer menggunakan Golang Embed, dapat dimanfaatkan dengan golang Embed
func TestFileServerGoEmbed(t *testing.T) {
	// !ini akan errror karena endpointya yang bisa di akses adalah /static/resources/index.html, dan /resources ini masuk karena folder resource kita
	//fileServer := http.FileServer(http.FS(resources))
	// *gunakan fs.Sub() untuk menghindari masuknya endpoint tambahan dari nama folder
	directory, _ := fs.Sub(resources, "resources")
	fileServer := http.FileServer(http.FS(directory))

	mux := http.NewServeMux()
	mux.Handle("/static/", http.StripPrefix("/static", fileServer))

	server := http.Server{
		Addr:    "localhost:8080",
		Handler: mux,
	}
	// *access this url using localhost:8080/static/index.html
	err := server.ListenAndServe()
	if err != nil {
		panic(err)
	}
}

// *01 FileServer
func TestFileServer(t *testing.T) {
	directory := http.Dir("./resources")
	fileServer := http.FileServer(directory)

	mux := http.NewServeMux()

	// !not found 404, karena FileServer membaca urlnya seperti /resources/static/index.html yang dimana itu merupakan endpoint yang salah & endpoint /static perlu di hapus
	// ?handle menggunakan http.StripPrefix() untuk menghapus prefix di url /static tersebut
	//mux.Handle("/static/", fileServer)
	mux.Handle("/static/", http.StripPrefix("/static", fileServer))

	server := http.Server{
		Addr:    "localhost:8080",
		Handler: mux,
	}

	err := server.ListenAndServe()
	if err != nil {
		panic(err)
	}
}
