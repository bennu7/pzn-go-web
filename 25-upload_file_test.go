package pzn_go_web

import (
	"bytes"
	_ "embed"
	"fmt"
	"github.com/google/uuid"
	"io"
	"mime/multipart"
	"net/http"
	"net/http/httptest"
	"os"
	"strings"
	"testing"
)

// *display form upload html
func uploadForm(w http.ResponseWriter, r *http.Request) {
	err := myTemplates.ExecuteTemplate(w, "upload.form.gohtml", nil)
	if err != nil {
		panic(err)
	}
}

// *uploading Handler
func Upload(w http.ResponseWriter, r *http.Request) {
	// *terlebih dahulu ia memparsing file nya, terdapat di dalam method FormFile()
	//r.ParseMultipartForm(32 << 20)
	file, fileHeader, err := r.FormFile("file")
	if err != nil {
		panic(err)
	}
	defer file.Close()
	generateUUID := uuid.New().String()
	fileName := strings.Join(strings.Split(fileHeader.Filename, " "), "~")
	//fileName := strings.Join(splitFile, "~")
	finalFileName := generateUUID + "-" + fileName

	// *lokasi simpan data di folder ./resources/
	fileDestination, err := os.Create("./resources/" + finalFileName)
	if err != nil {
		panic(err)
	}
	fmt.Println("/static/" + finalFileName)
	// *memindahkan data ke file destination folder resources
	_, err = io.Copy(fileDestination, file)
	if err != nil {
		panic(err)
	}

	name := r.PostFormValue("name")
	myTemplates.ExecuteTemplate(w, "upload.success.gohtml", map[string]any{
		"Name":     name,
		"File":     "/static/" + finalFileName,
		"NameFile": r.URL.Hostname() + "/static/" + finalFileName,
	})
}

func TestTemplateUploadFileServer(t *testing.T) {
	mux := http.NewServeMux()

	mux.HandleFunc("/", uploadForm)
	mux.HandleFunc("/upload", Upload)
	mux.Handle("/static/", http.StripPrefix("/static", http.FileServer(http.Dir("./resources"))))

	server := http.Server{
		Addr:    "localhost:8080",
		Handler: mux,
	}

	err := server.ListenAndServe()
	if err != nil {
		panic(err)
	}
}

//go:embed human.png
var fileHuman []byte

func TestTemplateUploadFileTesting(t *testing.T) {
	body := new(bytes.Buffer)
	writer := multipart.NewWriter(body)
	err := writer.WriteField("name", "lalu ibnu")
	if err != nil {
		panic(err)
	}

	file, err := writer.CreateFormFile("file", "human.png")
	if err != nil {
		panic(err)
	}
	write, err := file.Write(fileHuman)
	if err != nil {
		panic(err)
	}
	fmt.Println(write)
	writer.Close()

	request := httptest.NewRequest(http.MethodPost, "http://localhost:8080", body)
	request.Header.Set("Content-Type", writer.FormDataContentType())
	recorder := httptest.NewRecorder()

	Upload(recorder, request)

	response := recorder.Result()
	bodyResponse, err := io.ReadAll(response.Body)
	if err != nil {
		panic(err)
	}
	fmt.Println(string(bodyResponse))
}
