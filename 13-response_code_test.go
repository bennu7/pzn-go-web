package pzn_go_web

import (
	"fmt"
	"io"
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestResponseCode(t *testing.T) {
	request := httptest.NewRequest(http.MethodGet, "http://localhost:8080/hei?name=ben", nil)
	recorder := httptest.NewRecorder()

	responseCode(recorder, request)

	response := recorder.Result()
	code := response.StatusCode
	result, err := io.ReadAll(response.Body)
	if err != nil {
		panic(err)
	}
	fmt.Println(response.Status)
	fmt.Println("status code ", code, string(result))
}

func responseCode(w http.ResponseWriter, r *http.Request) {
	name := r.URL.Query().Get("name")
	if name == "" {
		// *kirim status code menggunakan responseWriter WriteHeader
		w.WriteHeader(http.StatusBadRequest)
		fmt.Fprintf(w, "name is empty")
	} else {
		w.WriteHeader(http.StatusOK)
		fmt.Fprintf(w, "hii %s berantem yok", name)
	}
}
