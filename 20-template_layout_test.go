package pzn_go_web

import (
	"fmt"
	"io"
	"net/http"
	"net/http/httptest"
	"testing"
	"text/template"
)

func templateLayout(w http.ResponseWriter, r *http.Request) {
	fileGoHtml := []string{
		"./templates/header.gohtml",
		"./templates/footer.gohtml",
		"./templates/layout.gohtml",
	}
	t := template.Must(template.ParseFiles(
		fileGoHtml...,
	))

	//t.ExecuteTemplate(w, "layout.gohtml", map[string]any{
	// *memanfaatkan template name dengan mengisi define pada layout utama dan diakhiri dengan end
	t.ExecuteTemplate(w, "layout", map[string]any{
		"Title":  "it's Layout go!",
		"Name":   "wkwk naniii",
		"Footer": "that's my footer!",
	})
}

func TestTemplateLayout(t *testing.T) {
	mux := http.NewServeMux()

	mux.HandleFunc("/", templateLayout)
	server := http.Server{
		Addr:    "localhost:8080",
		Handler: mux,
	}

	err := server.ListenAndServe()
	if err != nil {
		panic(err)
	}
}

func TestTemplateLayoutTesting(t *testing.T) {
	request := httptest.NewRequest(http.MethodGet, "http://localhost:8080/", nil)
	recorder := httptest.NewRecorder()

	templateLayout(recorder, request)

	response := recorder.Result()
	result, err := io.ReadAll(response.Body)
	if err != nil {
		panic(err)
	}
	fmt.Println(response.Status)
	fmt.Println(string(result))
}
