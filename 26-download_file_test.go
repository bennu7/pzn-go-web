package pzn_go_web

import (
	"fmt"
	"net/http"
	"testing"
)

func downlodFile(w http.ResponseWriter, r *http.Request) {
	fileName := r.URL.Query().Get("name")
	if fileName == "" {
		w.WriteHeader(http.StatusBadRequest)
		fmt.Fprint(w, "Bad Request")
		return
	}

	/*
	* source : https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Content-Disposition
	* kirim value menggunakan Content-Deposition, terdapat 3 jenis pengiriman data menggunakan Content-Deposition untuk parameter ke 2 pada method Add():
	* inline; -> ini tidak langsung di download, seperti ada preview
	* attachment; -> langsung di download tetapi tidak menggunakan nama file asli
	* attachment; namefile=NamaFile; -> langsung di download dgn menggunakan nama file asli
	 */
	w.Header().Add("Content-Disposition", "attachment; namefile=\""+fileName+"\"")
	http.ServeFile(w, r, "./resources/"+fileName)
}

func TestDownloadFileServer(t *testing.T) {
	mux := http.NewServeMux()

	mux.HandleFunc("/file", downlodFile)

	server := http.Server{
		Addr:    "localhost:8080",
		Handler: mux,
	}

	err := server.ListenAndServe()
	if err != nil {
		panic(err)
	}
}
